
def main():

    try:
        score = float(input('Enter score: '))
        print(compute_grade(score))

    except ValueError:
        print('Enter a number please\n')
        main()


def compute_grade(_score):
    if _score > 1.0:
        return 'Bad score'
    elif _score >= 0.9:
        return 'A'
    elif _score >= 0.8:
        return 'B'
    elif _score >= 0.7:
        return 'C'
    elif _score >= 0.6:
        return 'D'
    elif _score < 0.6:
        return 'F'
    else:
        return 'Bad score'


if __name__ == '__main__':
    main()


