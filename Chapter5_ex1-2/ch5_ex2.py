
numbers = []

print('\nPlease enter a number (or done to quit)')

while True:
    message = input('> ')

    if message == 'done':
        total = 0.0
        for item in numbers:
            total += item
        print('You entered:\n' + str(len(numbers)) + ' numbers\n' + 'total: ' + str(total) + '\nmaximum: ' +
              str(max(numbers)) + '\nminimum: ' + str(min(numbers)))
        print('Goodbye....')
        break

    try:
        message = float(message)
        numbers.append(message)

    except ValueError:
        print('invalid input')
