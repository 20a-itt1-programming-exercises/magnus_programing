message = ""
total = 0.0
count = 0
average = 0.0
print('\nPlease enter a number (or done to quit)')

while True:
    message = input('> ')
    if message == 'done':
        print('You entered:\n' + str(count) + ' numbers\n' + 'total: ' + str(total) + '\naverage: ' + str(average))
        print('Goodbye....')
        break
    try:
        message = float(message)
        total = total + message
        count = count + 1
        average = total / count

    except ValueError:
        print('invalid input')
