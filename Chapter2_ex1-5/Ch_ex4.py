Python 3.8.5 (tags/v3.8.5:580fbb0, Jul 20 2020, 15:43:08) [MSC v.1926 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> width = 17
>>> height = 12.0
>>> print('1. width//2:')
1. width//2:
>>> print(value: ' + str(width//2)
      
SyntaxError: invalid syntax
>>> print('value: ' + str(width//2))
value: 8
>>> print('type: ' + str(type(width/2.0)))
type: <class 'float'>
>>> print('\n')


>>> print('2. width/2.0:')
2. width/2.0:
>>> print('value: ' + str(width/2.0))
value: 8.5
>>> print('type: ' + str(type(width/2.0)))
type: <class 'float'>
>>> print('\n')


>>> print('3. height/3:')
3. height/3:
>>> print('value: ' + str(height/3))
value: 4.0
>>> print('type: ' + str(type(height/3)))
type: <class 'float'>
>>> print('\n')


>>> print('4. 1 + 2 * 5:')
4. 1 + 2 * 5:
>>> print('value: ' + str(1 + 2 * 5))
value: 11
>>> print('type: ' + str(type(1 + 2 * 5)))
type: <class 'int'>
>>> print ('\n')


>>> 