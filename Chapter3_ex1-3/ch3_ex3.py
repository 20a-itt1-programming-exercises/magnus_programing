try:
    score = float(input('Enter score: '))

    if score > 0.9 or score < 0:
        print('Bad score')

    elif score >= 0.9:
        print('Your grade is A')

    elif score >= 0.8:
        print('Your grade is B')

    elif score >= 0.7:
        print('Your grade is C')

    elif score >= 0.6:
        print('Your grade is D')

    elif score < 0.6:
        print('Your grade is E')

except:
    print('Enter numbers between 0.9 and 0')
    
